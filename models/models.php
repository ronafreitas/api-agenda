<?php

class Usuarios extends Illuminate\Database\Eloquent\Model{
  protected $table = 'T_USUARIOS';
}

class Agenda extends Illuminate\Database\Eloquent\Model{
	protected $table = 'T_AGENDA';

	protected $fillable = ['ID','Caption','Message','Location','Start','Finish','grupoId','Type','Options','ResourceID', 'State', 'LabelColor'];
	public $timestamps = false;
}

class Grupos extends Illuminate\Database\Eloquent\Model{
  protected $table = 'T_GRUPOS';
}
