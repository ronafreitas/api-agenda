<?php
/*
LOGIN
*/
$app->get('/v1/login', function() use ($app){
  $user = $app->request->get('user') ?? null;
  $pass = $app->request->get('pass') ?? null;
  if($user AND $pass){
    $results = Usuarios::where(array(
      'nome' => $user,
      'senha' => $pass
    ))->get(['cod_user','nome', 'grupoId']);
    return helpers::jsonResponse(false, '', $results);
  }else{
    return helpers::jsonResponse(true, 'erro', []);
  }
});

/*
endpoint para selecionar AGENDA, de acordo com usuário
*/
$app->get('/v1/agendas', function() use ($app){
  $grupoId = $app->request->get('grupoId') ?? null;

  $aanno = $app->request->get('ano');
  $mmees = $app->request->get('mes');
  $mmees++;
  $mesquery = $mmees ?? date('m');
  $anoquery = $aanno ?? date('Y');

  $dataInicio = $anoquery .'-'.'01'.'-'.$mesquery;
  $dateToTest = $anoquery.'-'.$mmees."-01";
  $lastday = date('t',strtotime($dateToTest));
  $dataFim = $anoquery.'-'.$lastday."-".$mmees;

  if($grupoId){
    $results = Agenda::where('grupoId','=',$grupoId)
    ->where('Start', '>=', $dataInicio)
    ->where('Start', '<=', $dataFim)
    ->get();
    return helpers::jsonResponse(false, '', $results);
  }else{
    return helpers::jsonResponse(true, 'erro', []);
  }
});

$app->get('/v1/agenda_by_data', function() use ($app){
  $data = $app->request->get('data') ?? null;
  $grupoId = $app->request->get('grupoId') ?? null;
  if($data){
    $dtconsult = date("Y-d-m", $data);
    //$dtconsult = date("Y-d-m", strtotime($data));
    $results = Agenda::where('Start','=',$dtconsult)
    ->where('grupoId', '=', $grupoId)
    ->get();
    return helpers::jsonResponse(false, '', $results);
  }else{
    return helpers::jsonResponse(true, 'erro', []);
  }
});

$app->get('/v1/grupos', function() use ($app){
  $results = Grupos::all();
  if($results){
    return helpers::jsonResponse(false, '', $results);
  }else{
    return helpers::jsonResponse(true, 'erro', []);
  }
});

$app->post('/v1/cadastrar_agenda', function() use ($app){
  $postdata = file_get_contents("php://input");
  if(isset($postdata)){
    $request = json_decode($postdata);

    $titulo = $request->Caption ?? null;
    $descricao = $request->Message ?? null;
    $local = $request->Location ?? null;
    $inicio = $request->Start ?? null;
    $fim = $request->Finish ?? null;
    $grupoId = $request->grupoId ?? null;
    $LabelColor = $request->LabelColor ?? null;

    $data = array(
      'Caption' => $titulo,
      'Message' => $descricao,
      'Location' => $local,
      'Start' => $inicio,
      'Finish' => $fim,
      'grupoId' => (int)$grupoId,
      'Type' => 0,
      'Options' => 2,
      'ResourceID' => 1,
      'State' => 0,
      'LabelColor' => $LabelColor
    );
    $results = Agenda::create($data);

    if($results){
      return helpers::jsonResponse(false, '', $results);
    }else{
      return helpers::jsonResponse(true, 'erro', []);
    }
  }else{
    return helpers::jsonResponse(true, 'erro', []);
  }
});

$app->post('/v1/atualizar_agenda', function() use ($app){
  $postdata = file_get_contents("php://input");
  if(isset($postdata)){
    $request = json_decode($postdata);

    $ID = $request->ID ?? null;
    $titulo = $request->Caption ?? null;
    $descricao = $request->Message ?? null;
    $local = $request->Location ?? null;
    $inicio = $request->Start ?? null;
    $fim = $request->Finish ?? null;
    $grupoId = $request->grupoId ?? null;
    $LabelColor = $request->LabelColor ?? null;

    $data = [
      'Caption' => $titulo,
      'Message' => $descricao,
      'Location' => $local,
      'Start' => $inicio,
      'Finish' => $fim,
      'grupoId' => (int)$grupoId,
      'Type' => 0,
      'Options' => 2,
      'ResourceID' => 1,
      'State' => 0,
      'LabelColor' => $LabelColor
    ];
    $results = Agenda::where('ID',$ID)->update($data);

    if($results){
      return helpers::jsonResponse(false, '', $results);
    }else{
      return helpers::jsonResponse(true, 'erro', []);
    }
  }else{
    return helpers::jsonResponse(true, 'erro', []);
  }
});