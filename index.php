<?php
require 'vendor/autoload.php';

/*$app = new \Slim\Slim(array(
  'debug' => true
));*/

$app = new \Slim\Slim();

$app->notFound(function () use ($app) {
    //$app->render('404.html');
    exit('erro');
});

$app->error(function (\Exception $e) use ($app) {
    //$app->render('error.php');
    exit('erro');
});

require_once "config/database.php";
require_once "helpers/helper.php";
require_once "models/models.php";
require_once "endpoints/endpoints.php";

$app->run();